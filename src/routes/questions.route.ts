import { Router } from 'express';
import QuestionsController from '../controllers/questions.controller';

class QuestionsRoutes {
	public router: Router = Router();
	public questionsController: QuestionsController;

	constructor() {
		this.questionsController = new QuestionsController();
		this.config();
	}

	config(): void{
		this.router.post('/create', this.questionsController.create);
		// this.router.post('/getCitiesByState', this.questionsController.getCitiesByState);
		// this.router.post('/', this.questionsController.getAllCountries);
	}
}

export default new QuestionsRoutes().router;
