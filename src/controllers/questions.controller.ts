import { Request, Response } from 'express'; // eslint-disable-line no-unused-vars
import { graphql } from 'graphql';
import { modules } from '../graphql';


class QuestionsController {

	public async create(req: Request, res: Response) {
		try {
			const { schema, context } = modules;

			const { fields } = req.body;
			const { data } = await graphql(schema, `${fields}`, context);
			res.json(data);
		} catch (err) {
			throw err;
		}
	}


	public async getAllCountries(req: Request, res: Response) {
		try {
			const { schema, context } = modules;

			const { fields } = req.body;
			const { data } = await graphql(schema, `${fields}`, context);
			res.json(data);
		} catch (err) {
			throw err;
		}
	}

	public async getStatesByCountry(req: Request, res: Response) {
		try {
			const { schema, context } = modules;
			const { fields } = req.body;
			const { data } = await graphql(schema, `${fields}`, context);
			res.json(data);
		} catch (err) {
			throw err;
		}
	}

	public async getCitiesByState(req: Request, res: Response) {
		try {
			const { schema, context } = modules;
			const { fields } = req.body;
			const { data } = await graphql(schema, `${fields}`, context);
			res.json(data);
		} catch (err) {
			throw err;
		}
	}
}

export default QuestionsController;
