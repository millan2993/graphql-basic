import { GraphQLModule } from '@graphql-modules/core';
import gql from 'graphql-tag';
import AnswerModel from '../models/answers.model';

export const AnswersModule = new GraphQLModule({
	typeDefs: gql`
		type Query {
			answersbyquestion(question: String): [Answer]!
		}

		type Answer {
			_id: String,
			question: String
			response: String
			date: 	String
			id_user: 	String
		}

		 type Mutation {
			createAnswer ( question: String, response: String, date: String, user: String ): Answer
			
			deleteAnswer (_id: String): Answer  
		 }
	`,

	resolvers: {
		Query: {
			answersbyquestion: async (_, { question }): Promise<any> => {
				const answers = await AnswerModel.findByQuestion(question);
				return answers;
			}
		},

		Mutation: {
			createAnswer: async (_, { question, response, user }): Promise<object> => {
				const answer = await AnswerModel.create(question, response, user);
				return answer;
			},

			deleteAnswer: async (_, { _id }) => {
				const deleted = await  AnswerModel.delete(_id);
				return deleted;
			}
		},

		Answer: {
			_id: 	  (answer): string => answer._id,
			response: (answer): string => answer.response,
			question: (answer): string => answer.question,
			date:	  (answer): string => answer.date,
			id_user:  (answer): string => answer.user
		}

	},

});
