import { GraphQLModule } from '@graphql-modules/core';
import gql from 'graphql-tag';
import QuestionsModel from '../models/questions.model';
import { find, filter } from 'lodash';

export const QuestionsModule = new GraphQLModule({
	typeDefs: gql`
		type Query {
			questions: [Question]
		}

		type Question {
			_id: String,
			id: Int,
			name: String,
			date: String,
			answers: [Answer]!
			users: [User]!
		}

    	type User {
			name: String
			email: String
		}

		type Answer {
			answer: String
			user: User
		}
		
		type QuestionCreate {
			name: String
		}
		
		 type Mutation {
			create ( name: String ): Question
			
			update (_id: String, name: String): Question
			
			delete (_id: String): Question
		 }
		
	`,

	resolvers: {
		Query: {
			questions: async (): Promise<any> => {
				const questions = await QuestionsModel.questions();
				return questions;
			}

		},

		Mutation: {
			create: async (_, { name }) => {
				const question = await QuestionsModel.create(name);
				return question;
			},

			update: async (_, { _id, name }): Promise<object> => {
				const question = await QuestionsModel.update(_id, name);
				return question;
			},

			delete: async (_, { _id }) => {
				const deleted = await  QuestionsModel.delete(_id);
				return deleted;
			}
		},

		Question: {
			_id: (question): string => question._id,
			id: (question): string => question.id,
			name: (question): string => question.name,
			date: (question): string => question.date
		}

	},

});
