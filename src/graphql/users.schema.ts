import { GraphQLModule } from '@graphql-modules/core';
import gql from 'graphql-tag';
import UsersModel from '../models/users.model';

export const UsersModule = new GraphQLModule({
	typeDefs: gql`
		type Query {
			users: [User]
		}

		type User {
			_id: String,
			first_name: String,
			last_name: String,
			email: String
		}

		 type Mutation {
			createUser ( first_name: String, last_name: String, email: String ): User
			
			updateUser ( _id: String, first_name: String, last_name: String, email: String): User
			
			deleteUser (_id: String): User  
		 }
		
	`,

	resolvers: {
		Query: {
			users: async (): Promise<any> => {
				const users = await UsersModel.all();
				return users;
			}
		},

		Mutation: {
			createUser: async (_, { first_name, last_name, email }): Promise<object> => {
				const user = await UsersModel.create(first_name, last_name, email);
				return user;
			},

			updateUser: async (_, { _id, first_name, last_name, email }): Promise<object> => {
				const user = await UsersModel.update(_id, first_name, last_name, email);
				return user;
			},

			deleteUser: async (_, { _id }) => {
				const deleted = await  UsersModel.delete(_id);
				return deleted;
			}
		},

		User: {
			_id: (user): string => user._id,
			first_name: (user): string => user.first_name,
			last_name: (user): string => user.last_name,
			email: (user): string => user.email
		}

	},

});
