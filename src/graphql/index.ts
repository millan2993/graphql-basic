import { GraphQLModule } from '@graphql-modules/core';
import { UsersModule } from './users.schema';
import { QuestionsModule } from './questions.schema';
import {AnswersModule} from "./answers.schema";


export const modules = new GraphQLModule({
	imports: [
		UsersModule,
		QuestionsModule,
		AnswersModule
	],
});
