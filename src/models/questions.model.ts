// /* import nedb from '../data/db.conn'; */

import db from "../db";
import {ObjectId} from "mongodb";

class QuestionsModel {

	public static async questions(): Promise<object[]> {
		try {
			const questions = await db.then(
				conn => conn.collection('questions')
					.find()
					.toArray());

			return questions;
		} catch (err) {
			throw err;
		}
	}


	public static async create(name: string) {
		try {
			const question = await db.then(conn => conn.collection('questions')
				.insertOne({
					name: name,
					date: new Date().toLocaleString()
				}, error => {
					console.log(error);
				}
			));

			return question;

		} catch (err) {
			throw err;
		}
	}


	public static update(id: string, name: string): Promise<object> {

		return new Promise<object>(async (resolve, reject) => {
			await db.then(
			con => con.collection('questions').findOneAndUpdate(
				{_id: new ObjectId(id) },
				{
					'$set': {'name': name},
				},
				{
					returnOriginal: false
				},
				function (err, question) {
					resolve(question.value);
				}
			));
		});
	};


	public static async delete(id: string) {

		const deleted = await db.then(
			con => con.collection('questions').deleteOne(
				{ _id: new ObjectId(id) }
			)
		);

		return deleted;
	}

}

export default QuestionsModel;
