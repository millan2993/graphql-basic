import db from "../db";
import {ObjectId} from "mongodb";

class AnswerModel {

	public static async findByQuestion(question: string): Promise<object[]> {
		try {
			const answers = await db.then(
				conn => conn.collection('answers')
					.find({ "question": new ObjectId(question) })
					.toArray());

			return answers;

		} catch (err) {
			throw err;
		}
	}


	public static async create(question: string, response: string, user: string): Promise<object> {

		return new Promise<object>(async (resolve, reject) => {
			await db.then(conn => conn.collection('answers').insertOne(
				{
					"question": new ObjectId(question),
					"response": response,
					"user"	: new ObjectId(user),
					"date"  : new Date().toLocaleString()

				}, function(err, answer) {
					resolve(answer.ops[0]);
				}
			));
		});
	}


	public static async delete(id: string) {

		const deleted = await db.then(
			con => con.collection('answers').deleteOne(
				{ _id: new ObjectId(id) }
			)
		);

		return deleted;
	}

}

export default AnswerModel;
