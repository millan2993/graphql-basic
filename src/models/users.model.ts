import db from "../db";
import {ObjectId} from "mongodb";

class UsersModel {

	public static async all(): Promise<object[]> {
		try {
			const questions = await db.then(
				conn => conn.collection('users')
					.find()
					.toArray());

			return questions;
		} catch (err) {
			throw err;
		}
	}


	public static async create(first_name: string, last_name: string, email: string): Promise<object> {

		return new Promise<object>(async (resolve, reject) => {
			await db.then(conn => conn.collection('users')
				.insertOne({
						"first_name": first_name,
						"last_name": last_name,
						"email": email
					}, function(err, user) {
						resolve(user.ops[0]);
					}
				));
		});
	}



	public static update(id: string, first_name: string, last_name: string, email: string): Promise<object> {

		return new Promise<object>(async (resolve, reject) => {
			await db.then(
			con => con.collection('users').findOneAndUpdate(
				{_id: new ObjectId(id) },
				{
					'$set': {
						'first_name': first_name,
						'last_name' : last_name,
						'email'		: email
					},
				},
				{
					returnOriginal: false
				},
				function (err, question) {
					resolve(question.value);
				}
			));
		});
	};


	public static async delete(id: string) {

		const deleted = await db.then(
			con => con.collection('users').deleteOne(
				{ _id: new ObjectId(id) }
			)
		);

		return deleted;
	}

}

export default UsersModel;
