import express, { Application } from 'express'; // eslint-disable-line no-unused-vars
import config from 'config';
import cors from 'cors';
import { ApolloServer } from 'apollo-server-express';
// import usersRoutes from './routes/users.route';
import questionRoutes from './routes/questions.route';
// import * as uni from './app';
import { modules } from './graphql';

const { schema, context } = modules;

class Server {
    public app: Application;
    public server: ApolloServer;
    public port: number;
    public path: string;

    constructor() {
        this.path = '/graphql';
        this.port = 3000;
        this.app = express();
        this.server = new ApolloServer({ schema, context, introspection: true });
        this.config();
        this.routes();
    }

    config(): void {
        this.app.set('port', this.port || 3000);
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
    }

    routes(): void {
        this.server.applyMiddleware({ app: this.app, path: this.path });
        // this.app.use('/api/users', usersRoutes);
        this.app.use('/api/questions', questionRoutes);
        // this.app.get('*', (uni.handleRender));
    }

    start(): void {
        this.app.listen(this.app.get('port'), (): void => {
            console.log(`🚀 Server ready at http://localhost:${this.app.get('port')}`);
        });
    }
}

new Server().start();
